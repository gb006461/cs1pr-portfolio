#pragma once

#include <entity.hpp>

class Player : public Entity {
public:
	// Constructor/Destructor
	Player(Stage* stage);
	~Player() = default;

	// Virtual Override Functions
	void Tick() override;

private:
	SDL_Texture* mSpriteSheet[2];

	Stage* mStage;
};