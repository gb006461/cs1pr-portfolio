/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include <pch.hpp>

#include <stage.hpp>
#include <platform.hpp>

Platform::Platform(Stage* stage, const char *line) :
	mStage(stage) {

	mStage->getEntityListTail()->setNext(reinterpret_cast<Entity*>(this));
	mStage->setEntityListTail(reinterpret_cast<Entity*>(this));

	sscanf(line, "%*s %f %f %f %f", &iPosition.sx, &iPosition.sy, &iPosition.ex, &iPosition.ey);

	iHealth = 1;

	iPosition.x = iPosition.sx;
	iPosition.y = iPosition.sy;

	iTexture = mStage->LoadTexture("gfx/platform.png");
	SDL_QueryTexture(iTexture, NULL, NULL, &iWidth, &iHeight);
	iFlags = EF_SOLID+EF_WEIGHTLESS+EF_PUSH;
}

void Platform::Tick() {
	if (abs(iPosition.x - iPosition.sx) < PLATFORM_SPEED && abs(iPosition.y - iPosition.sy) < PLATFORM_SPEED) {
		calcSlope(iPosition.ex, iPosition.ey, iPosition.x, iPosition.y, &iPosition.dx, &iPosition.dy);

		iPosition.dx *= PLATFORM_SPEED;
		iPosition.dy *= PLATFORM_SPEED;
	}

	if (abs(iPosition.x - iPosition.ex) < PLATFORM_SPEED && abs(iPosition.y - iPosition.ey) < PLATFORM_SPEED) {
		calcSlope(iPosition.sx, iPosition.sy, iPosition.x, iPosition.y, &iPosition.dx, &iPosition.dy);

		iPosition.dx *= PLATFORM_SPEED;
		iPosition.dy *= PLATFORM_SPEED;
	}
}
