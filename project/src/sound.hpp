class SoundManager {
public:
	SoundManager();
	~SoundManager() = default;

	// Public Utility Methods
	void LoadMusic(const char* filename);

	void PlayMusic(int loop);
	void _PlaySound(int id, int channel);

private:
	// Private Utility Methods
	void LoadSounds();
	


	// Private Member Variables
	Mix_Chunk* mSounds[SND_MAX];
	Mix_Music* mMusic;
};
