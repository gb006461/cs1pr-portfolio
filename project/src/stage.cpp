/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include <pch.hpp>

#include <stage.hpp>

#include <text.hpp>

Stage::Stage() {

	mEntityListTail = &mEntityListHead;

	mPlayer = new Player(this);
	mEntityManager = new EntityManager(this);
	mTextRenderer = new TextRenderer(this);

}

Stage::~Stage() {
	delete mPlayer;
	delete mEntityManager;
}

void Stage::Logic() {
	PrepareScene();

	GetInput();

	mPlayer->Tick();

	mEntityManager->UpdateEntities();

	UpdateCamera();
}

void Stage::Draw(Map* map) {
	SDL_SetRenderDrawColor(iRenderer, 128, 192, 255, 255);
	SDL_RenderFillRect(iRenderer, NULL);

	map->DrawMap();

	mEntityManager->DrawEntities();

	DrawHud();
}

void Stage::DrawHud() {
	SDL_Rect r;

	r.x = 0;
	r.y = 0;
	r.w = SCREEN_WIDTH;
	r.h = 35;

	SDL_SetRenderDrawBlendMode(iRenderer, SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(iRenderer, 0, 0, 0, 196);
	SDL_RenderFillRect(iRenderer, &r);
	SDL_SetRenderDrawBlendMode(iRenderer, SDL_BLENDMODE_NONE);

	mTextRenderer->_DrawText(SCREEN_WIDTH - 5, 5, 255, 255, 255, TEXT_RIGHT, "PIZZA %d/%d", mPizzaFound, mPizzaTotal);
}

void Stage::UpdateCamera() {
	mCamera.x = (int)mPlayer->getPosition().x + (mPlayer->getWidth() / 2);
	mCamera.y = (int)mPlayer->getPosition().y + (mPlayer->getHeight() / 2);

	mCamera.x -= (SCREEN_WIDTH / 2);
	mCamera.y -= (SCREEN_HEIGHT / 2);

	mCamera.x = MIN(MAX(mCamera.x, 0), (MAP_WIDTH * TILE_SIZE) - SCREEN_WIDTH);
	mCamera.y = MIN(MAX(mCamera.y, 0), (MAP_HEIGHT * TILE_SIZE) - SCREEN_HEIGHT);
}
