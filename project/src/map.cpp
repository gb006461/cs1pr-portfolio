/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include <pch.hpp>

#include <stage.hpp>

Map::Map(Stage* stage) :
	mStage(stage) {

	LoadTiles();
	LoadMap("data/map01.dat");
}

void Map::DrawMap() {
	int x, y, n, x1, x2, y1, y2, mx, my;

	x1 = (mStage->getCamera().x % TILE_SIZE) * -1;
	x2 = x1 + MAP_RENDER_WIDTH * TILE_SIZE + (x1 == 0 ? 0 : TILE_SIZE);

	y1 = (mStage->getCamera().y % TILE_SIZE) * -1;
	y2 = y1 + MAP_RENDER_HEIGHT * TILE_SIZE + (y1 == 0 ? 0 : TILE_SIZE);

	mx = mStage->getCamera().x / TILE_SIZE;
	my = mStage->getCamera().y / TILE_SIZE;

	for (y = y1 ; y < y2 ; y += TILE_SIZE) {
		for (x = x1 ; x < x2 ; x += TILE_SIZE) {
			if (Map::IsInsideMap(mx, my)) {
				std::array<std::array<int, MAP_HEIGHT>, MAP_WIDTH> map;
				map = mStage->getMap();

				n = map[mx][my];

				if (n > 0) {
					mStage->Blit(mTiles[n], x, y, 0);
				}
			}

			mx++;
		}

		mx = mStage->getCamera().x / TILE_SIZE;

		my++;
	}
}

void Map::LoadTiles() {
	int i;
	char filename[MAX_FILENAME_LENGTH];

	for (i = 1 ; i <= MAX_TILES ; i++) {
		sprintf(filename, "gfx/tile%d.png", i);

		mTiles[i] = mStage->LoadTexture(filename);
	}
}

void Map::LoadMap(const char *filename) {
	char *data, *p;
	int x, y;

	data = readFile(filename);

	p = data;

	for (y = 0 ; y < MAP_HEIGHT ; y++) {
		for (x = 0 ; x < MAP_WIDTH ; x++) {
			sscanf(p, "%d", &mStage->getMap()[x][y]);

			do {p++;} while (*p != ' ' && *p != '\n');
		}

		y = y;
	}

	free(data);
}

int Map::IsInsideMap(int x, int y) {
	return x >= 0 && y >= 0 && x < MAP_WIDTH && y < MAP_HEIGHT;
}
