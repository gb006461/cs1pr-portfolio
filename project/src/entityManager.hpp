#pragma once

extern class Entity;

class EntityManager {
public:
	EntityManager(Stage* stage);
	~EntityManager() = default;

	void Move(Entity* e);
	void Push(Entity* e, float dx, float dy);
	void MoveToWorld(Entity* e, float dx, float dy);
	void MoveToEntities(Entity* e, float dx, float dy);
	void LoadEntities(const char* filename);
	void AddEntityFromLine(char* line);
	void DrawEntities();

	void UpdateEntities();

private:
	Stage* mStage;
};