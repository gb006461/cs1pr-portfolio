#include <pch.hpp>

#include <app.hpp>

Application::Application() {
	iTextureListTail = &iTextureListHead;
	
	InitSDL();
	InitGame();
}

Application::~Application() {
	SDL_DestroyRenderer(iRenderer);

	SDL_DestroyWindow(iWindow);

	SDL_Quit();
}

void Application::InitSDL() {
	int rendererFlags, windowFlags;

	rendererFlags = SDL_RENDERER_ACCELERATED;

	windowFlags = 0;

	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("Couldn't initialize SDL: %s\n", SDL_GetError());
		exit(1);
	}

	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024) == -1) {
		printf("Couldn't initialize SDL Mixer\n");
		exit(1);
	}

	Mix_AllocateChannels(MAX_SND_CHANNELS);

	iWindow = SDL_CreateWindow("Pete's Pizza Party 6", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, windowFlags);

	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");

	iRenderer = SDL_CreateRenderer(iWindow, -1, rendererFlags);

	IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG);

	SDL_ShowCursor(0);
}

void Application::InitGame() {
	mSoundManager.LoadMusic("music/one_0.mp3");
	mSoundManager.PlayMusic(1);
}

void Application::GetInput() {
	SDL_Event event;

	while (SDL_PollEvent(&event)) {
		switch (event.type) {
		case SDL_QUIT:
			exit(0);
			break;

		case SDL_KEYDOWN:
			DoKeyDown(&event.key);
			break;

		case SDL_KEYUP:
			DoKeyUp(&event.key);
			break;

		default:
			break;
		}
	}
}

void Application::DoKeyUp(SDL_KeyboardEvent* event) {
	if (event->repeat == 0 && event->keysym.scancode < MAX_KEYBOARD_KEYS) {
		iKeyboard[event->keysym.scancode] = 0;
	}
}

void Application::DoKeyDown(SDL_KeyboardEvent* event) {
	if (event->repeat == 0 && event->keysym.scancode < MAX_KEYBOARD_KEYS) {
		iKeyboard[event->keysym.scancode] = 1;
	}
}

void Application::PrepareScene() {
	SDL_SetRenderDrawColor(iRenderer, 0, 0, 0, 255);
	SDL_RenderClear(iRenderer);
}

void Application::PresentScene() {
	SDL_RenderPresent(iRenderer);
}

void Application::Blit(SDL_Texture* texture, int x, int y, int center) {
	SDL_Rect dest;

	dest.x = x;
	dest.y = y;
	SDL_QueryTexture(texture, NULL, NULL, &dest.w, &dest.h);

	if (center) {
		dest.x -= dest.w / 2;
		dest.y -= dest.h / 2;
	}

	SDL_RenderCopy(iRenderer, texture, NULL, &dest);
}

void Application::BlitRect(SDL_Texture* texture, SDL_Rect* src, int x, int y) {
	SDL_Rect dest;

	dest.x = x;
	dest.y = y;
	dest.w = src->w;
	dest.h = src->h;

	SDL_RenderCopy(iRenderer, texture, src, &dest);
}

void Application::DrawRect(SDL_Rect rect, SDL_Color colour) {
	SDL_SetRenderDrawColor(iRenderer, colour.r, colour.g, colour.b, colour.a);

	SDL_RenderFillRect(iRenderer, &rect);

}

SDL_Texture* Application::getTexture(const char* name) {
	for (Texture* texture = iTextureListHead.next; texture != NULL; texture = texture->next) {
		if (strcmp(texture->name, name) == 0) {
			return texture->texture;
		}
	}

	return NULL;
}

void Application::AddTextureToCache(const char* name, SDL_Texture* sdlTexture) {
	Texture* texture;

	texture = (Texture*)malloc(sizeof(Texture));
	memset(texture, 0, sizeof(Texture));
	iTextureListTail->next = texture;
	iTextureListTail = texture;

	STRNCPY(texture->name, name, MAX_NAME_LENGTH);
	texture->texture = sdlTexture;
}

SDL_Texture* Application::LoadTexture(const char* filename) {
	SDL_Texture* texture;

	texture = getTexture(filename);

	if (texture == NULL) {
		SDL_LogMessage(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_INFO, "Loading %s ...", filename);
		texture = IMG_LoadTexture(iRenderer, filename);

		if (texture == NULL) {
			SDL_LogMessage(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_INFO, "ERROR loading %s: %s", filename, IMG_GetError());
		}

		AddTextureToCache(filename, texture);
	}

	return texture;
}
