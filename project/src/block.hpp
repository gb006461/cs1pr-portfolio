#pragma once

#include <entity.hpp>
#include <stage.hpp>

class Block : private Entity {
public:
	// Constructor/Destructor
	Block(Stage* stage, const char* line);
	Block() = default;
	~Block() = default;

private:
	Stage* mStage;

};
