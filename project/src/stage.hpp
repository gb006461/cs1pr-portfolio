#include <app.hpp>
#include <map.hpp>
#include <player.hpp>
#include <entityManager.hpp>

extern class TextRenderer;

class Stage : public Application {
public:
	using MapArray = std::array<std::array<int, MAP_HEIGHT>, MAP_WIDTH>;

	// Constructor / Destructor
	Stage();
	~Stage();

	// Public Utility Methods
	void DrawHud();
	void UpdateCamera();

	void IncrementPizzaTotal() { mPizzaTotal++; }
	void IncrementFoundPizzas() { mPizzaFound++; }

	// Virtual Function Overrides
	void Logic() override;
	void Draw(Map* map) override;

	// Getters
	SoundManager& getSoundManager() { return mSoundManager; }

	Entity& getEntityListHead() { return mEntityListHead; }
	Entity* getEntityListTail() { return mEntityListTail; }

	SDL_Point& getCamera() { return mCamera; }
	MapArray& getMap() { return mMapArray; }

	Player* getPlayer() { return mPlayer; }

	int getKeyboardValue(SDL_Scancode scancode) { return iKeyboard[scancode]; }

	int getPizzaTotal() { return mPizzaTotal; }
	int getFoundPizzas() { return mPizzaFound; }

	bool getRunning() { return iRunning; }

	// Setters
	void setEntityListHead(Entity& listHead) { mEntityListHead = listHead; }
	void setEntityListTail(Entity* listTail) { mEntityListTail = listTail; }
	void setKeyboardValue(SDL_Scancode scancode, int value) { iKeyboard[scancode] = value; }
	void setRunning(bool running) { iRunning = running; }

private:
	// Stage Variables
	Player* mPlayer;
	EntityManager* mEntityManager;
	TextRenderer* mTextRenderer;

	// Map Variables
	SDL_Point mCamera;
	MapArray mMapArray;
	
	// Entity Linked List
	Entity mEntityListHead;
	Entity* mEntityListTail;

	// Game Variables
	int mPizzaTotal, mPizzaFound;

};