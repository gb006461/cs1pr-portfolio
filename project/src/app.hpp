#pragma once

class Map;

#include <sound.hpp>

class Application {
public:
	typedef struct Texture {
		char name[MAX_NAME_LENGTH];
		SDL_Texture* texture;
		Texture* next;
	} Texture;

	typedef struct Surface {
		SDL_Surface* texture;
		Surface* next;
	} Surface;

	// Constructor/Destructor
	Application();
	~Application();

	// Virtual Functions
	virtual void Logic() { }
	virtual void Draw(Map* map) { }

	// Public Utility Methods
	void GetInput();

	void PrepareScene();
	void PresentScene();
	void Blit(SDL_Texture* texture, int x, int y, int center);
	void BlitRect(SDL_Texture* texture, SDL_Rect* src, int x, int y);
	void DrawRect(SDL_Rect rect, SDL_Color colour);

	SDL_Texture* LoadTexture(const char* filename);

	// Getters
	SDL_Texture* getTexture(const char* name);
	

protected:
	SDL_Renderer* iRenderer;
	SDL_Window* iWindow;

	SoundManager mSoundManager;

	int iKeyboard[MAX_KEYBOARD_KEYS];

	Texture iTextureListHead, *iTextureListTail;
	Surface iSurfaceListHead, *iSurfaceListTail;
	
	bool iRunning = true;

private:
	// Private Utility Methods
	void InitSDL();
	void InitGame();

	void DoKeyUp(SDL_KeyboardEvent* event);
	void DoKeyDown(SDL_KeyboardEvent* event);

	void AddTextureToCache(const char* name, SDL_Texture* sdlTexture);


};
