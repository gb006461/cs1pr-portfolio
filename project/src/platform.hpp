#pragma once

#include <entity.hpp>

class Platform : private Entity {
public:
	// Constructor/Destructor
	Platform(Stage* stage, const char* line);
	Platform() = default;
	~Platform() = default;

	void Tick() override;

private:
	Stage* mStage;
};