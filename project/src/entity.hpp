#pragma once

class Entity {
public:
	// Constructor/Destructor
	Entity() = default;
	~Entity() = default;

	typedef struct EntityPosition {
		float x, y;
		float ex, ey;
		float sx, sy;
		float dx, dy;

	} EntityPosition;

	// Virtual Functions
	virtual void Tick() { }
	virtual void Touch(Entity* other) { }

	// Getters
	int getHealth() { return iHealth; }
	EntityPosition& getPosition() { return iPosition; }
	int getWidth() { return iWidth; }
	int getHeight() { return iHeight; }
	int getIsOnGround() { return iIsOnGround; }

	Entity* getNext() { return iNext; }
	Entity* getRiding() { return iRiding; }
	SDL_Texture* getTexture() { return iTexture; }

	long getFlags() { return iFlags; }

	// Setters
	void setHealth(int health) { iHealth = health; }
	void setPosition(float x, float y) { iPosition.x = x; iPosition.y = y; }
	void setIsOnGround(int onGround) { iIsOnGround = onGround; }

	void setNext(Entity* next) { iNext = next; }
	void setRiding(Entity* riding) { iRiding = riding; }

protected:
	// Member Variables
	EntityPosition iPosition;
	int iWidth, iHeight;

	std::vector<Entity*> iEntityTriggers;

	int iHealth;
	int iIsOnGround;
	float iValue;

	SDL_Texture* iTexture;

	Entity* iRiding;
	Entity* iNext;

	long iFlags;

};
