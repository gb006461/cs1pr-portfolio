#pragma once

#include <windows.h>

#include <ctype.h>
#include <math.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <array>
#include <tuple>
#include <iostream>

#include <defs.h>

extern class Player;
extern class Stage;

char* readFile(const char* filename);
int collision(int x1, int y1, int w1, int h1, int x2, int y2, int w2, int h2);
void calcSlope(int x1, int y1, int x2, int y2, float* dx, float* dy);
void cleanup(void);
void drawText(int x, int y, int r, int g, int b, int align, const char* format, ...);
void initFonts(void);
void initSounds(void);
void loadMusic(const char* filename);
void playMusic(int loop);
void playSound(int id, int channel);
