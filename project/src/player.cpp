/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include <pch.hpp>

#include <stage.hpp>

#include <player.hpp>

Player::Player(Stage* stage) :
	mStage(stage) {

	// Up-cast Player Entity to an Entity object
	stage->getEntityListTail()->setNext(reinterpret_cast<Entity*>(this));
	stage->setEntityListTail(reinterpret_cast<Entity*>(this));

	iHealth = 1;

	mSpriteSheet[0] = mStage->LoadTexture("gfx/pete01.png");
	mSpriteSheet[1] = mStage->LoadTexture("gfx/pete02.png");
	
	iTexture = mSpriteSheet[0];

	SDL_QueryTexture(iTexture, NULL, NULL, &iWidth, &iHeight);
}

void Player::Tick() {
	iPosition.dx = 0;

	if (mStage->getKeyboardValue(SDL_SCANCODE_A)) {
		iPosition.dx = -PLAYER_MOVE_SPEED;

		iTexture = mSpriteSheet[1];
	}

	if (mStage->getKeyboardValue(SDL_SCANCODE_D)) {
		iPosition.dx = PLAYER_MOVE_SPEED;

		iTexture = mSpriteSheet[0];
	}

	if (mStage->getKeyboardValue(SDL_SCANCODE_SPACE) && iIsOnGround) {
		iRiding = NULL;

		iPosition.dy = -20;

		mStage->getSoundManager()._PlaySound(SND_JUMP, CH_PLAYER);
	}

	if (mStage->getKeyboardValue(SDL_SCANCODE_R)) {
		iPosition.x = iPosition.y = 0;

		mStage->setKeyboardValue(SDL_SCANCODE_R, 0);
	}
}