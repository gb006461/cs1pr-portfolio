class TextRenderer {
public:
	TextRenderer(Stage* stage);
	~TextRenderer() = default;

	// Public Utility Methods
	void _DrawText(int x, int y, int r, int g, int b, int align, const char* format, ...);

private:
	Stage* mStage;

	SDL_Texture* mFontTexture;
	char mDrawTextBuffer[MAX_LINE_LENGTH];
};
