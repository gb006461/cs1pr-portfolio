/*
Copyright (C) 2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include <pch.hpp>

#include <sound.hpp>

SoundManager::SoundManager() {
	memset(mSounds, 0, sizeof(Mix_Chunk*) * SND_MAX);

	mMusic = NULL;

	LoadSounds();
}

void SoundManager::LoadMusic(const char *filename) {
	if (mMusic != NULL) {
		Mix_HaltMusic();
		Mix_FreeMusic(mMusic);
		mMusic = NULL;
	}

	mMusic = Mix_LoadMUS(filename);
}

void SoundManager::PlayMusic(int loop) {
	Mix_PlayMusic(mMusic, (loop) ? -1 : 0);
}

void SoundManager::_PlaySound(int id, int channel) {
	Mix_PlayChannel(channel, mSounds[id], 0);
}

void SoundManager::LoadSounds() {
	mSounds[SND_JUMP] = Mix_LoadWAV("sound/331381__qubodup__public-domain-jump-sound.ogg");
	
	if (mSounds[SND_JUMP] == NULL) {
		printf("Couldn't load sound/331381__qubodup__public-domain-jump-sound.ogg\n");
	} else {
		printf("Loaded sound successfully\n");
	}

	mSounds[SND_PIZZA] = Mix_LoadWAV("sound/90134__pierrecartoons1979__found-item.ogg");
	mSounds[SND_PIZZA_DONE] = Mix_LoadWAV("sound/449069__ricniclas__fanfare.ogg");
}
