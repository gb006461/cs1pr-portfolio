/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include <pch.hpp>

#include <stage.hpp>
#include <particleSystem.hpp>

static void capFrameRate(long *then, float *remainder);

int main(int argc, char *argv[]) {
	long then;
	float remainder;

	Stage stage;
	Map map(&stage);

	then = SDL_GetTicks();
	remainder = 0;

	ParticleSystem particleSystem(&stage, { 50, 200 });

	ParticleSystem::ParticleCreateInfo createInfo = { };
	createInfo.count = 100;

	createInfo.minColourRange = { 1, 1, 215, 255 };
	createInfo.maxColourRange = { 2, 2, 230, 255 };

	createInfo.minLifeTime = 75;
	createInfo.maxLifeTime = 200;

	createInfo.maxSize = 20;

	particleSystem.Generate(createInfo);

	long finishTime = 0;
	bool endGame = false;
	while (stage.getRunning() || then < finishTime) {
		stage.Logic();
		particleSystem.Update({ static_cast<int>(stage.getPlayer()->getPosition().x), static_cast<int>(stage.getPlayer()->getPosition().y) });

		stage.Draw(&map);
		particleSystem.DrawParticles();

		stage.PresentScene();

		capFrameRate(&then, &remainder);

		if (!stage.getRunning() && !endGame) {
			finishTime = then + 2500;

			createInfo.minColourRange = { 1, 215, 1, 255 };
			createInfo.maxColourRange = { 2, 230, 2, 255 };

			particleSystem.Generate(createInfo);
			
			endGame = true;
		}
	}

	exit(1);

	return 0;
}

static void capFrameRate(long *then, float *remainder) {
	long wait, frameTime;

	wait = 16 + *remainder;

	*remainder -= (int)*remainder;

	frameTime = SDL_GetTicks() - *then;

	wait -= frameTime;

	if (wait < 1) {
		wait = 1;
	}

	SDL_Delay(wait);

	*remainder += 0.667;

	*then = SDL_GetTicks();
}
