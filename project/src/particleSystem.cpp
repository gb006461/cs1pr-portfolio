#include <pch.hpp>

#include <app.hpp>
#include <stage.hpp>

#include <particleSystem.hpp>

ParticleSystem::ParticleSystem(Stage* stage, SDL_Point origin) :
	mStage(stage),
	mOrigin(origin) { }

void ParticleSystem::Update(SDL_Point originPoint) {
	mOrigin = originPoint;

	Update();
}

void ParticleSystem::Update() {
	for (Particle& particle : mParticles) {
		particle.life--;

		if (particle.life > 0) { // Particle still alive
			if (particle.velocityVector.x > 0) particle.x += particle.velocityVector.x -= 0.001f;
			if (particle.velocityVector.x < 0) particle.x += particle.velocityVector.x += 0.001f;
			if(particle.velocityVector.y > -10.0f) particle.velocityVector.y -= 0.15f;
			particle.y -= particle.velocityVector.y;

		} else {
			particle.alive = false;
		}
	}
}

void ParticleSystem::DrawParticles() {
	for (Particle particle : mParticles) { // Loop through all particles in the particle system
		if (particle.alive) { // If the particle is alive
			mStage->DrawRect({ particle.x, particle.y, particle.size, particle.size }, particle.colour);

		}
	}
}

void ParticleSystem::Generate(ParticleCreateInfo& particleCreateInfo) {
	for (int i = 0; i < particleCreateInfo.count; i++) { 
		bool generatedParticle = false;
		
		for (Particle& particle : mParticles) { // See whether an existing particle is not alive
			if (!particle.alive) { // If so, reanimate it
				particle.x = mOrigin.x;
				particle.y = mOrigin.y;

				particle.size = (rand() + 5) % particleCreateInfo.maxSize;
				particle.velocityVector = { static_cast<float>((rand() % 19 + (-9))), static_cast<float>(rand() % 12) };
				particle.colour = { static_cast<uint8_t>((rand() + particleCreateInfo.minColourRange.r) % particleCreateInfo.maxColourRange.r),
									static_cast<uint8_t>((rand() + particleCreateInfo.minColourRange.g) % particleCreateInfo.maxColourRange.g),
									static_cast<uint8_t>((rand() + particleCreateInfo.minColourRange.b) % particleCreateInfo.maxColourRange.b),
									255 };

				particle.life = (rand() + particleCreateInfo.minLifeTime) % particleCreateInfo.maxLifeTime;
				particle.alive = true;

				generatedParticle = true;
			}

		}
		
		// If no existing particle was dead, push back a new particle
		if (!generatedParticle) {
			Particle newParticle = { };
			newParticle.x = mOrigin.x;
			newParticle.y = mOrigin.y;

			newParticle.size = (rand() + 5) % particleCreateInfo.maxSize;
			newParticle.velocityVector = { static_cast<float>((rand() % 19 + (-9))), static_cast<float>(rand() % 12) };
			newParticle.colour = { static_cast<uint8_t>((rand() + particleCreateInfo.minColourRange.r) % particleCreateInfo.maxColourRange.r),
								   static_cast<uint8_t>((rand() + particleCreateInfo.minColourRange.g) % particleCreateInfo.maxColourRange.g),
								   static_cast<uint8_t>((rand() + particleCreateInfo.minColourRange.b) % particleCreateInfo.maxColourRange.b),
								255 };

			newParticle.life = (rand() + particleCreateInfo.minLifeTime) % particleCreateInfo.maxLifeTime;
			newParticle.alive = true;

			// Push back the new particle information 
			mParticles.push_back(newParticle);
		}
	}
}

