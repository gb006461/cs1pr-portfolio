class Map {
public:
	// Constructor/Destructor
	Map(Stage* stage);
	~Map() = default;

	// Public Methods
	void DrawMap();

	static int IsInsideMap(int x, int y);

private:
	Stage* mStage;

	// Private Utility Methods
	void LoadTiles(void);
	void LoadMap(const char* filename);
	
	SDL_Texture* mTiles[MAX_TILES];
};