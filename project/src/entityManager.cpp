/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include <pch.hpp>

#include <entity.hpp>

#include <entityManager.hpp>

#include <block.hpp>
#include <pizza.hpp>
#include <platform.hpp>

EntityManager::EntityManager(Stage* stage) :
	mStage(stage) {

	LoadEntities("data/ents01.dat");
}

void EntityManager::UpdateEntities() {
	int count = 0;

	// Head of stage entity list
	Entity* previous = &mStage->getEntityListHead();

	for (Entity* entity = mStage->getEntityListHead().getNext(); entity != NULL; entity = entity->getNext()) {
		entity->Tick();

		Move(entity);

		if (entity->getHealth() <= 0) {
			if (entity == mStage->getEntityListTail()) {
				mStage->setEntityListTail(previous);
			}

			previous->setNext(entity->getNext());

			delete(entity);

			entity = previous;
		}

		previous = entity;
	}	

	for (Entity* entity = mStage->getEntityListHead().getNext(); entity != NULL; entity = entity->getNext()) {
		// Entity has a riding entity
		if (entity->getRiding() != NULL) {
			entity->getPosition().x += entity->getRiding()->getPosition().dx;

			Push(entity, entity->getRiding()->getPosition().dx, 0);
		}

		entity->setPosition(MIN(MAX(entity->getPosition().x, 0), MAP_WIDTH * TILE_SIZE), MIN(MAX(entity->getPosition().y, 0), MAP_HEIGHT * TILE_SIZE));

	}

}

void EntityManager::Move(Entity* entity) {
	if (!(entity->getFlags() & EF_WEIGHTLESS)) {
		entity->getPosition().dy += 1.5;
		entity->getPosition().dy = MAX(MIN(entity->getPosition().dy, 18), -999);
	}

	if (entity->getRiding() != NULL && entity->getRiding()->getPosition().dy > 0) {
		entity->getPosition().dy = entity->getRiding()->getPosition().dy + 1;
	}

	// Remove the entity riding this entity
	entity->setRiding(NULL);

	// Reset IsOnGround flag
	entity->setIsOnGround(0);

	// Move character by delta values
	entity->getPosition().x += entity->getPosition().dx;
	Push(entity, entity->getPosition().dx, 0);

	entity->getPosition().y += entity->getPosition().dy;
	Push(entity, 0, entity->getPosition().dy);
}

void EntityManager::Push(Entity* entity, float dx, float dy) {
	MoveToWorld(entity, dx, dy);

	MoveToEntities(entity, dx, dy);
}

void EntityManager::MoveToWorld(Entity* entity, float dx, float dy) {

	int mx, my, hit, adj;

	if (dx != 0) {
		// Get the bounding box x position
		mx = dx > 0 ? (entity->getPosition().x + entity->getWidth()) : entity->getPosition().x;

		// Convert it to map units
		mx /= TILE_SIZE;
		
		// Set the hit flag to 0
		hit = 0;

		// Get the bounding box bottom y position
		my = entity->getPosition().y / TILE_SIZE;

		// If entity is not inside the map, or tile entity is on is not air, set the hit flag to true
		if (!Map::IsInsideMap(mx, my) || mStage->getMap()[mx][my] != 0) {
			hit = 1;
		}

		// Get the bounding box top y position
		my = (entity->getPosition().y + entity->getHeight() - 1) / TILE_SIZE;

		// If entity is not inside the map, or tile entity is on is not air, set the hit flag to true
		if (!Map::IsInsideMap(mx, my) || mStage->getMap()[mx][my] != 0) {
			hit = 1;
		}

		
		if (hit) {
			// Create the offset to find the most adjacent tile position
			adj = dx > 0 ? -entity->getWidth() : TILE_SIZE;

			// Place the entity at the safe position
			entity->getPosition().x = (mx * TILE_SIZE) + adj;

			// Set velocity to 0
			entity->getPosition().dx = 0;
		}
	}

	if (dy != 0) {
		my = dy > 0 ? (entity->getPosition().y + entity->getHeight()) : entity->getPosition().y;
		my /= TILE_SIZE;

		mx = entity->getPosition().x / TILE_SIZE;

		hit = 0;

		if (!Map::IsInsideMap(mx, my) || mStage->getMap()[mx][my] != 0) {
			hit = 1;
		}

		mx = (entity->getPosition().x + entity->getWidth() - 1) / TILE_SIZE;

		if (!Map::IsInsideMap(mx, my) || mStage->getMap()[mx][my] != 0) {
			hit = 1;
		}

		if (hit) {
			adj = dy > 0 ? -entity->getHeight() : TILE_SIZE;

			entity->getPosition().y = (my * TILE_SIZE) + adj;

			entity->getPosition().dy = 0;

			entity->setIsOnGround(dy > 0);
		}
	}
}

void EntityManager::MoveToEntities(Entity* entity, float dx, float dy) {
	Entity* other;
	int adj;

	for (other = mStage->getEntityListHead().getNext(); other != NULL; other = other->getNext()) {
		if (other != entity && collision(entity->getPosition().x, entity->getPosition().y, 
										 entity->getWidth(), entity->getHeight(), 
										 other->getPosition().x, other->getPosition().y, 
			                             other->getWidth(), other->getHeight())) {

			if (other->getFlags() & EF_SOLID) {
				if (dy != 0) {
					adj = dy > 0 ? -entity->getHeight() : other->getHeight();

					entity->getPosition().y = other->getPosition().y + adj;

					entity->getPosition().dy = 0;

					if (dy > 0) {
						entity->setIsOnGround(1);

						entity->setRiding(other);
					}
				}

				if (dx != 0) {
					adj = dx > 0 ? -entity->getWidth() : other->getHeight();

					entity->getPosition().x = other->getPosition().x + adj;

					entity->getPosition().dx = 0;
				}

			} else if (entity->getFlags() & EF_PUSH) {
				other->getPosition().x += entity->getPosition().dx;
				Push(other, entity->getPosition().dx, 0);

				other->getPosition().y += entity->getPosition().dy;
				Push(other, 0, entity->getPosition().dy);
			}

			entity->Touch(other);
		}
	}
}

void EntityManager::DrawEntities() {
	for (Entity* entity = mStage->getEntityListHead().getNext(); entity != NULL; entity = entity->getNext())	{
		mStage->Blit(entity->getTexture(), entity->getPosition().x - mStage->getCamera().x, entity->getPosition().y - mStage->getCamera().y, 0);
	}
}

void EntityManager::LoadEntities(const char* filename) {
	char line[MAX_LINE_LENGTH];
	char* data, * p;
	int n;

	data = readFile(filename);

	p = data;

	n = 0;

	memset(line, '\0', MAX_LINE_LENGTH);

	while (*p) {
		if (*p == '\n') {
			AddEntityFromLine(line);
			memset(line, '\0', MAX_LINE_LENGTH);
			n = 0;
		} else {
			line[n++] = *p;
		}

		p++;
	}

	free(data);
}

void EntityManager::AddEntityFromLine(char* line) {
	char name[MAX_NAME_LENGTH];

	sscanf(line, "%s", name);

	if (strcmp(name, "BLOCK") == 0) {
		Block* block = new Block(mStage, line);

	} else if (strcmp(name, "PLATFORM") == 0) {
		Platform* platform = new Platform(mStage, line);
		
	} else if (strcmp(name, "PIZZA") == 0) {
		Pizza* pizza = new Pizza(mStage, line);

	} else {
		SDL_LogMessage(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_WARN, "Unknown entity '%s'", line);
	}
}
