#pragma once

#include <entity.hpp>

class Pizza : private Entity {
public:
	// Constructor/Destructor
	Pizza(Stage* stage, const char* line);
	Pizza() = default;
	~Pizza() = default;

	void Tick() override;
	void Touch(Entity* other) override;

private:
	Stage* mStage;

};