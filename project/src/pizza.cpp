/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include <pch.hpp>

#include <stage.hpp>

#include <pizza.hpp>

Pizza::Pizza(Stage* stage, const char *line) :
	mStage(stage) {

	Entity *e;

	mStage->getEntityListTail()->setNext(reinterpret_cast<Entity*>(this));
	mStage->setEntityListTail(reinterpret_cast<Entity*>(this));

	sscanf(line, "%*s %f %f", &iPosition.x, &iPosition.y);

	iHealth = 1;

	iEntityTriggers.push_back(mStage->getPlayer());

	iTexture = mStage->LoadTexture("gfx/pizza.png");
	SDL_QueryTexture(iTexture, NULL, NULL, &iWidth, &iHeight);
	iFlags = EF_WEIGHTLESS;

	mStage->IncrementPizzaTotal();

}

void Pizza::Tick(void) {
	iValue += 0.1;

	iPosition.y += sin(iValue);
}

void Pizza::Touch(Entity* other) {
	for (Entity* entity : iEntityTriggers) {
		if (iHealth > 0 && other == entity) {
			iHealth = 0;

			mStage->IncrementFoundPizzas();

			if (mStage->getFoundPizzas() == mStage->getPizzaTotal()) {
				mStage->getSoundManager()._PlaySound(SND_PIZZA_DONE, CH_PIZZA);
				mStage->setRunning(false);
			} else {
				mStage->getSoundManager()._PlaySound(SND_PIZZA, CH_PIZZA);
			}
		}
	}
}
