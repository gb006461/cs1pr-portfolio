class ParticleSystem {
public:
	struct Vector2f {
		float x, y;
	};

	struct Particle {
		int x, y;
		int size;
		
		Vector2f velocityVector;
		SDL_Color colour;

		int life;
		int alive;
	};

	struct ParticleCreateInfo {
		int count;
		int maxSize;
		SDL_Color minColourRange;
		SDL_Color maxColourRange;
		int minLifeTime;
		int maxLifeTime;
		Vector2f velocityVector;
	};


	// Constructor/Destructor
	ParticleSystem(Stage* mStage, SDL_Point origin);
	~ParticleSystem() = default;

	// Public Utility Methods
	void Update();
	void Update(SDL_Point originPoint);
	void DrawParticles();
	void Generate(ParticleCreateInfo& particleCreateInfo);

private:
	Stage* mStage;

	SDL_Point mOrigin;

	std::vector<Particle> mParticles;

};